
#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>
#include <ctime>

#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "flandmark_detector.h"

using namespace cv;
using namespace std;

int IM_WIDTH = 150;
int IM_HEIGHT = 150;

void register_face(Mat &face, double *landmarks){

	/// Calculate warp matrix
	Point2d eye_center;
	eye_center.x = (landmarks[2] + landmarks[10] + landmarks[4] + landmarks[12]) * 0.25;
	eye_center.y = (landmarks[3] + landmarks[11] + landmarks[5] + landmarks[13]) * 0.25;

	double dx = (-landmarks[2] - landmarks[10] + landmarks[4] + landmarks[12]) * 0.5f;
	double dy = (-landmarks[3] - landmarks[11] + landmarks[5] + landmarks[13]) * 0.5f;
	double len = sqrt(dx*dx + dy*dy);
	double angle = atan2(dy, dx) * 180.0 / CV_PI;
	double scale = 0.6 * IM_WIDTH / len;

	//Rotation
	Mat rot_mat = getRotationMatrix2D(eye_center, angle, scale);

	// Translation
	rot_mat.at<double>(0, 2) += face.cols * 0.5f - eye_center.x;
	rot_mat.at<double>(1, 2) += face.rows * 0.27f - eye_center.y;	//vertical position of eyes

	warpAffine(face, face, rot_mat, face.size(), INTER_CUBIC, BORDER_CONSTANT, Scalar(128));

}

void getDescriptor(Mat face, double *landmarks, Mat &descriptors){

	vector<KeyPoint> keyPoints;
	// Mouth left corner (x, y) = (6, 7)
	keyPoints.push_back(KeyPoint(Point(int(landmarks[6]), int(landmarks[7])), 20.0));
	// Mouth right corner (x, y) = (8, 9)
	keyPoints.push_back(KeyPoint(Point(int(landmarks[8]), int(landmarks[9])), 20.0));
	// Point between eyebrow (x, y) = (16, 17)
	keyPoints.push_back(KeyPoint(Point(int(landmarks[16]), int(landmarks[17])), 20.0));
	// Point left of nose (x, y) = (18, 19)
	keyPoints.push_back(KeyPoint(Point(int(landmarks[18]), int(landmarks[19])), 20.0));
	// Point right of nose (x, y) = (20, 21)
	keyPoints.push_back(KeyPoint(Point(int(landmarks[20]), int(landmarks[21])), 20.0));
	// Center of left eye (x, y) = (22, 23)
	keyPoints.push_back(KeyPoint(Point(int(landmarks[22]), int(landmarks[23])), 20.0));
	// Center of right eye (x, y) = (24, 25)
	keyPoints.push_back(KeyPoint(Point(int(landmarks[24]), int(landmarks[25])), 20.0));

	SurfDescriptorExtractor extractor;
	extractor.extended = true;
	extractor.compute(face, keyPoints, descriptors);

	descriptors = descriptors.reshape(0, 1);
	// cout << descriptors << endl;

}

void addLandmark(Mat &face, double *landmarks){

	// Point between eyebrow (x, y) = (16, 17)
	landmarks[16] = (landmarks[2] + landmarks[10] + landmarks[4] + landmarks[12]) * 0.25;
	landmarks[17] = (landmarks[3] + landmarks[11] + landmarks[5] + landmarks[13]) * 0.25 - (landmarks[2] - landmarks[10] + landmarks[12] - landmarks[4]) * 0.4;
	if (landmarks[17] < 1)
		landmarks[17] = 1;

	// Point left of nose (x, y) = (18, 19)
	landmarks[18] = (landmarks[2] + landmarks[10]) * 0.5 + (landmarks[6] + landmarks[8]) * 0.5 - landmarks[0];
	landmarks[19] = landmarks[15] + 0.3 * (landmarks[7] - landmarks[15]);

	// Point right of nose (x, y) = (20, 21)
	landmarks[20] = (landmarks[4] + landmarks[12]) * 0.5 + (landmarks[6] + landmarks[8]) * 0.5 - landmarks[0];
	landmarks[21] = landmarks[15] + 0.3 * (landmarks[9] - landmarks[15]);

	// Center of left eye (x, y) = (22, 23)
	landmarks[22] = (landmarks[2] + landmarks[10]) * 0.5;
	landmarks[23] = (landmarks[3] + landmarks[11]) * 0.5;

	// Center of right eye (x, y) = (24, 25)
	landmarks[24] = (landmarks[4] + landmarks[12]) * 0.5;
	landmarks[25] = (landmarks[5] + landmarks[13]) * 0.5;

}

void markFeatures(Mat &face, FLANDMARK_Model *modelLandmark, double *landmarks){
	// Mouth left corner (x, y) = (6, 7)
	circle(face, Point(int(landmarks[6]), int(landmarks[7])), 3, 1);
	// Mouth right corner (x, y) = (8, 9)
	circle(face, Point(int(landmarks[8]), int(landmarks[9])), 3, 1);
	// Point between eyebrow (x, y) = (16, 17)
	circle(face, Point(int(landmarks[16]), int(landmarks[17])), 3, 1);
	// Point left of nose (x, y) = (18, 19)
	circle(face, Point(int(landmarks[18]), int(landmarks[19])), 3, 1);
	// Point right of nose (x, y) = (20, 21)
	circle(face, Point(int(landmarks[20]), int(landmarks[21])), 3, 1);
	// Center of left eye (x, y) = (22, 23)
	circle(face, Point(int(landmarks[22]), int(landmarks[23])), 3, 1);
	// Center of right eye (x, y) = (24, 25)
	circle(face, Point(int(landmarks[24]), int(landmarks[25])), 3, 1);
}


//enlargeRegion
void enlargeRegion(Rect_<int> &rect, int cols, int rows) {
	rect.x -= cvRound(rect.width * 0.1);
	rect.width = cvRound(rect.width * 1.2);
	rect.height = cvRound(rect.height * 1.2);

	// make sure not to exceed the borders
	if (rect.x < 1)
		rect.x = 1;
	if (rect.y < 1)
		rect.y = 1;
	if (rect.x + rect.width >= cols)
		rect.width = cols - 1 - rect.x;
	if (rect.y + rect.height >= rows)
		rect.height = rows - 1 - rect.y;
	if (rect.width < rect.height)
		rect.height = rect.width;
	else
		rect.width = rect.height;

}

// add training emotion
void addTrainEmotion(string foldername, int emotion_id, Mat &desc_train, Mat &label_train, CascadeClassifier frontalDetector, FLANDMARK_Model * modelLandmark){

	int bbox[4] = { 1, 1, IM_WIDTH - 1, IM_HEIGHT - 1 };
	double * landmarks = new double[30];
	int bw_margin[2] = { 0, 0 };

	for (int i = 1;; i++){
		string fn = foldername + "/" + to_string(i) + ".jpg";
		Mat img = imread(fn, CV_LOAD_IMAGE_GRAYSCALE);
		if (img.empty())
			break;
		vector< Rect_<int> > img_rect;
		frontalDetector.detectMultiScale(img, img_rect, 1.1f, 10, CASCADE_FIND_BIGGEST_OBJECT, Size(100, 100));
		if (img_rect.size() != 0){
			enlargeRegion(img_rect[0], img.cols, img.rows);
			Mat face = img(img_rect[0]);
			resize(face, face, Size(IM_WIDTH, IM_HEIGHT), 1.0, 1.0, INTER_CUBIC);

			// face landmark
			IplImage *faceIpl = &(IplImage)face;
			if (flandmark_detect(faceIpl, bbox, modelLandmark, landmarks, bw_margin)){
				printf("Error during detection.\n");
			}
			else{
				// add landmark between the eyebrows
				addLandmark(face, landmarks);
				// Get descriptors!
				Mat descriptors;
				getDescriptor(face, landmarks, descriptors);

				if ((desc_train.cols != 0) && (descriptors.cols != desc_train.cols))
					continue;
				desc_train.push_back(descriptors);
				label_train.push_back(emotion_id);

				markFeatures(face, modelLandmark, landmarks);
				imshow("training", face);
				waitKey(1);
			}
		}
	}
}

int main(int argc, const char *argv[])
{
	/// Input file names here
	// Set filenames to be read in
	// string filename_faces = "faces.txt";
	string filename_bodyDetector = "haarcascade_mcs_upperbody.xml";
	string filename_frontalDetector = "haarcascade_frontalface_alt_tree.xml";
	string filename_frontalDetectorWeak = "lbpcascade_frontalface.xml";

	// string filename_smileDetector = "haarcascade_smile.xml";
	int cameraID = 0;

	/// Load detectors
	CascadeClassifier bodyDetector, frontalDetector, frontalDetectorWeak, smileDetector;
	bodyDetector.load(filename_bodyDetector);
	frontalDetector.load(filename_frontalDetector);
	frontalDetectorWeak.load(filename_frontalDetectorWeak);

	/// Initialize feature point detectors
	FLANDMARK_Model * modelLandmark = flandmark_init("flandmark_model.dat");
	int bbox[4] = { 1, 1, IM_WIDTH - 1, IM_HEIGHT - 1};
	double * landmarks = new double[30];
	int bw_margin[2] = {0, 0};

	//*********Load directly from file
	 //CvSVM svmClassifier;
	 //svmClassifier.load("emotion4new128.dat");

	 
	///******Training Emotion*************
	Mat desc_train;
	Mat label_train;
	addTrainEmotion("nosmile", 0, desc_train, label_train, frontalDetector, modelLandmark);
	addTrainEmotion("smile", 1, desc_train, label_train, frontalDetector, modelLandmark);
	addTrainEmotion("surprise", 2, desc_train, label_train, frontalDetector, modelLandmark);
	addTrainEmotion("negative", 3, desc_train, label_train, frontalDetector, modelLandmark);

	desc_train.convertTo(desc_train, CV_32FC1);
	label_train.convertTo(label_train, CV_32FC1);

	/// SVM 
	CvSVMParams SVM_params;
	SVM_params.kernel_type = CvSVM::LINEAR;

	CvSVM svmClassifier(desc_train, label_train, Mat(), Mat(), SVM_params);
	svmClassifier.save("emotion4new128.dat");

	///******Main Loop*************
	/// Open camera
	 VideoCapture cap(cameraID);

	// Check if we can use this device at all:
	if (!cap.isOpened()) {
		cerr << "Camera ID " << cameraID << "cannot be opened." << endl;
		exit(1);
	}

	/// Holds the current frame from the Video device:
	Mat frame;	
	for (;;) {
		cap >> frame;
		if (!frame.data) break;
		// Clone the current frame:
		Mat original = frame.clone();
		// Convert the current frame to grayscale:
		Mat gray;
		cvtColor(original, gray, CV_BGR2GRAY);
		
		vector< Rect_<int> > bodies_rect, frontal_rect, frontalWeak_rect;
		Rect_<int> temp_rect;
		bodyDetector.detectMultiScale(gray, bodies_rect, 1.1f, 3, 0, Size(100, 100));
		for (int i = 0; i < bodies_rect.size(); i++){
			// Shrink the region
			bodies_rect[i].x = bodies_rect[i].x + cvRound(bodies_rect[i].height * 0.2);
			bodies_rect[i].y = bodies_rect[i].y + cvRound(bodies_rect[i].height * 0.1);
			bodies_rect[i].width = cvRound(bodies_rect[i].width * 0.6);
			bodies_rect[i].height = cvRound(bodies_rect[i].height * 0.6);

			// For each body, search head
			Size minFaceSize = Size(cvRound(bodies_rect[i].width * 0.4), cvRound(cvRound(bodies_rect[i].height * 0.4)));
			frontalDetectorWeak.detectMultiScale(gray(bodies_rect[i]), frontalWeak_rect, 1.1f, 3, CASCADE_FIND_BIGGEST_OBJECT, minFaceSize);

			// If good frontal faces exist, do identification and emotional analysis
			if (frontalWeak_rect.size() != 0){
				temp_rect.x = bodies_rect[i].x + frontalWeak_rect[0].x;
				temp_rect.y = bodies_rect[i].y + frontalWeak_rect[0].y;
				temp_rect.width = frontalWeak_rect[0].width;
				temp_rect.height = frontalWeak_rect[0].height;
				//rectangle(frame, temp_rect, CV_RGB(255, 0, 0), 2);

				// enlarge region of frontal face a little bit before search landmarks
				enlargeRegion(temp_rect, frame.cols, frame.rows);
				rectangle(frame, temp_rect, CV_RGB(255, 0, 0), 2);
				Mat profileFace = gray(temp_rect);

				// face landmark
				resize(profileFace, profileFace, Size(IM_WIDTH, IM_HEIGHT), 1.0, 1.0, INTER_CUBIC);
				IplImage *img = &(IplImage)profileFace;
				if (flandmark_detect(img, bbox, modelLandmark, landmarks, bw_margin)){
					printf("Error during detection.\n");
				}
				else{
					// add a landmark between the eyebrows
					addLandmark(profileFace, landmarks);
					// Get descriptors!
					Mat descriptors;
					getDescriptor(profileFace, landmarks, descriptors);
					markFeatures(profileFace, modelLandmark, landmarks);
					imshow("face", profileFace);

					descriptors.convertTo(descriptors, CV_32FC1);
					if (descriptors.cols != svmClassifier.get_var_count())
						continue;
					int emo = (int)svmClassifier.predict(descriptors);
					string box_text = format("Emotion: %d", emo);
					putText(frame, box_text, Point(temp_rect.tl().x, temp_rect.tl().y), FONT_HERSHEY_PLAIN, 3.0, CV_RGB(255, 255, 0), 2);
				}
			}
		}
		imshow("Color Image", frame);
		waitKey(1);

	}

	// free memory
	flandmark_free(modelLandmark);
	delete[] landmarks;
	return 0;
}